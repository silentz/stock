"""
@Author: wenrongli
@Date: 2014-11-18
@Version: V1.0
"""

# !/user/python
# -*- coding: gb2312 -*-
import os
import sys
import getopt
from WindPy import *
from datetime import *


class LoadHisInfo:
    def __init__(self):
        self.OutputDir = r'E:'
        self.StockCodeList = []

    def StartWind(self):
        w.start()

    def LoadStockList(self):
        res = w.wset("SectorConstituent", "date=;sector=ȫ��A��")
        if res.ErrorCode != 0:
            print('Error[' + str(res.ErrorCode) + '][load stockcode list fail]\n')
            sys.exit()
        for i in range(0, len(res.Data[0])):
            for k in range(0, len(res.Fields)):
                if res.Fields[k] == 'date':
                    date = res.Data[k][i]
                if res.Fields[k] == "wind_code":
                    code = res.Data[k][i]
                if res.Fields[k] == "sec_name":
                    name = res.Data[k][i]
                    self.StockCodeList.append(code)

    def LoadHisInfo(self):
        for code in self.StockCodeList:
            outputPath = self.OutputDir + code + r'.txt'
            data = w.wsd(code, "open,high,low,close", "19900101", "", showblank=0)
            if data.ErrorCode != 0:
                print('Error[' + str(data.ErrorCode) + '][load history info fail]\n')
                sys.exit()
            fOutputObj = open(outputPath.encode('gbk'), 'a')
        for i in range(0, len(data.Data[0])):
            date = str(data.Times[i])
        for k in range(0, len(data.Fields)):
            if data.Fields[k] == "OPEN":
                openPrice = data.Data[k][i]
            if data.Fields[k] == "LOW":
                lowPrice = data.Data[k][i]
            if data.Fields[k] == "HIGH":
                highPrice = data.Data[k][i]
            if data.Fields[k] == "CLOSE":
                closePrice = data.Data[k][i]
            if (openPrice == 0) or (lowPrice == 0) or (highPrice == 0) or (closePrice == 0):
                continue
            result = date[0:10] + "," + str(openPrice) + "," + str(highPrice) + "," + str(lowPrice) + "," + str(
                closePrice) + "\n"
            fOutputObj.write(result)
            fOutputObj.close()

    def Run(self):
        self.StartWind()
        self.LoadStockList()
        self.LoadHisInfo()


if __name__ == '__main__':
    loadInfoObj = LoadHisInfo()
if len(sys.argv) < 3:
    print("python LoadWindInfo.py -o outPutDir")
sys.exit()
opts, args = getopt.getopt(sys.argv[1:], "o:")
for op, value in opts:
    if op == "-o":
        loadInfoObj.OutputDir = value
    else:
        print("python LoadWindInfo.py -o outPutDir")
        sys.exit()

loadInfoObj.Run()