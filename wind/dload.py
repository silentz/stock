#!/user/python
# -*- coding: utf-8 -*-
from WindPy import *
import sys

reload(sys)
sys.setdefaultencoding('utf-8')

w.start()


# realtime=w.wsq("600000.SH,000001.SZ","rt_last,rt_last_vol")#


for i in range(0, 200):
    name=str(600000 + i)
    data = w.wsd(name + ".SH", "open,high,low,close,vwap,volume,turn", "2014-02-01", "",showblank=0)
    f = open("./files/"+name, 'w')
    time = data.Times
    value = data.Data
    if type(value) is list:
        col = len(value)
        row = len(value[0])
        for r in range(row):
            f.write(name+"\t")
            for c in range(col):
                f.write((str(value[c][r]))+ "\t"),
            f.write(str(time[r].date()) + "\n")
    f.flush()
    f.close()
    print(str(600000 + i) + ".SH")

for i in range(1, 101):
    if i < 10:
        name = "00000" + str(i)
    elif i < 100:
        name = "0000" + str(i)
    else:
        name = "000" + str(i)
    data = w.wsd(name+".SZ", "open,high,low,close,vwap,volume,turn", "2014-02-01", "", "TradingCalendar = SZSE",showblank=0)
    f = open("./files/"+name, 'w')
    time = data.Times
    value = data.Data
    # print data
    if type(value) is list:
        col = len(value)
        row = len(value[0])
        for r in range(row):
            f.write(name+"\t")
            for c in range(col):
                f.write((str(value[c][r])) + "\t"),
                # f.write(str(value[c][r])+"\t"),
            f.write(str(time[r].date()) + "\n")

        f.write("\n")

    print(name)
    f.flush()
    f.close()


def read():
    fid = open('600000.SH', 'r')
    s = fid.readline()
    print(s.decode('gb2312')).encode('utf-8')