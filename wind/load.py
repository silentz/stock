# fid=open('600000.SH','r')
# s=fid.readline()
# print(s.decode('gb2312')).encode('utf-8')


from WindPy import *
import sys
import os

reload(sys)
sys.setdefaultencoding('utf-8')

w.start()
f = open("./code", 'w')

for i in range(0, 200):
    name=str(600000 + i)
    data = w.wsd(name + ".SH", 'sec_name,sec_englishname,trade_code,mkt,sec_type,exch_eng,val_pe_deducted_ttm', "2015-01-09", "2015-01-09",showblank=0)
    time = data.Times
    value = data.Data

    if value[0][0] == 0.0:
        print(data)
        os.remove("./files/"+name)
        continue

    if type(value) is list:
        col = len(value)
        row = len(value[0])
        for r in range(row):
            for c in range(col):
                f.write((str(value[c][r]).decode('utf-8')).encode('gb2312')+ "\t"),
                # f.write(str(value[c][r])+ "\t"),
            f.write(str(time[r].date()) + "\n")
    f.flush()
    print(str(600000 + i) + ".SH")

for i in range(1, 101):
    if i < 10:
        name = "00000" + str(i) + ".SZ"
    elif i < 100:
        name = "0000" + str(i) + ".SZ"
    else:
        name = "000" + str(i) + ".SZ"
    data = w.wsd(name, 'sec_name,sec_englishname,trade_code,mkt,sec_type,exch_eng,val_pe_deducted_ttm', "2015-01-09", "2015-01-09", "TradingCalendar = SZSE",showblank=0)
    time = data.Times
    value = data.Data

    if value[0][0] == 0.0:
        print(data)
        os.remove("./files/"+name)
        continue

    if type(value) is list:
        col = len(value)
        row = len(value[0])
        for r in range(row):
            for c in range(col):
                f.write((str(value[c][r]).decode('utf-8')).encode('gb2312')+ "\t"),
                # f.write(str(value[c][r])+ "\t"),
            f.write(str(time[r].date()) + "\n")
    f.flush()
    print(name)

f.close()